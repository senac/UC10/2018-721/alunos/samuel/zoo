
package ZoologicoTematico;

public class Pato implements Ave{

    @Override
    public void poeOvo() {
        System.out.println("Sim poe ovo");
    }

    @Override
    public void andar() {
        System.out.println("Sisca");
    }

    @Override
    public void falar() {
        System.out.println("Quaaake");
    }
    
}
