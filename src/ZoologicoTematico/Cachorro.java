
package ZoologicoTematico;


public class Cachorro implements Mamifero{

    @Override
    public void Mamar() {
        System.out.println("Com a boca");
    }

    @Override
    public void andar() {
        System.out.println("Anda sobre quatro patas");
    }

    @Override
    public void falar() {
        System.out.println("Aaauu Aaauu");
    }
    
}