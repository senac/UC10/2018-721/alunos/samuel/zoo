
package ZoologicoTematico;

public class Galinha implements Ave{

    @Override
    public void poeOvo() {
        System.out.println("poe ovo");
    }

    @Override
    public void andar() {
        System.out.println("Siscando");
    }

    @Override
    public void falar() {
        System.out.println("Pó pô póóó");
    }
    
}
